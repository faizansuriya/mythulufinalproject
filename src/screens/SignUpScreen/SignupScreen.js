import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  AsyncStorage
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import styles from "./SignupScreenStyle";
import * as authUtils from "../../utils/auth.util";
import services from "../../services";
import { get } from "lodash";
import {
  emailConstraint,
  passwordConstraint,
  nameConstraint
} from "../../utils/constraints";
var validate = require("validate.js");

const _backgroundImage = require("../../resources/assets/splash-background.png");
const _logoImage = require("../../resources/assets/logo.png");

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      password: "",
      confirmPassword: ""
    };
  }

  register = async () => {
    var constraints = {
      name: nameConstraint,
      email: emailConstraint,
      password: passwordConstraint
    };
    var validation = validate(
      {
        name: this.state.name,
        email: this.state.email,
        password: this.state.password
      },
      constraints,
      { format: "flat" }
    );
    //   console.log(validation);
    if (validation) {
      alert(validation[0]);
      return;
    }

    try {
      if (this.state.password !== this.state.confirmPassword) {
        alert("Password Not Match");
        return;
      }
      const token = await services.auth.registerUser(this.state);
      console.log(token);
      await authUtils.setToken(token);
      if (token.data.status) {
        this.props.navigation.navigate("profileScreen");
      } else {
        alert(get(token, "data.error.message", "User Already Exists"));
      }
    } catch (err) {
      //console.log(err);
      alert(get(err, "response.data.error.message", "User Already Exists "));
    }
  };

  // _loadIntialState  = async () => {
  //     try{
  //     let value =  await AsyncStorage.getItem('user');
  //     if (value !=null ){
  //         this.props.navigation.navigate ("profileScreen");
  //     }
  // }
  //     catch(error){
  //         alert(error);
  //     }
  // }

  // componentDidMount(){

  //     this._loadIntialState();
  // }

  // register = () => {

  //     fetch("http://159.89.41.180/mythulu/api/v1/register" , {
  //         method : 'POST',
  //         headers : {
  //             'Accept'  : 'application/json',
  //             'Content-Type' : 'application/json'
  //         },

  //         body : JSON.stringify ({
  //             name : this.state.name,
  //             email : this.state.email,
  //             password : this.state.password,
  //             confirmpassword : this.state.confirmPassword

  //         })

  //     })

  //         .then((response) => response.json())
  //         .then ((res) => {
  //             if (res.status === true) {
  //                 this.props.navigation.navigate("loginScreen");
  //             }

  //             else {
  //                 alert(JSON.stringify(res));
  //             }
  //         })
  //     }

  render() {
    return (
      <View style={styles.container}>
        <Image source={_backgroundImage} style={styles.backgroundImage} />
        <KeyboardAwareScrollView>
          <View style={styles.logocontainer}>
            <Image source={_logoImage} />
          </View>

          <View style={styles.formcontainer}>
            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Full Name"
              placeholderTextColor="#ffffff"
              autoCapitalize="none"
              onChangeText={name => this.setState({ name })}
            />
            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Email"
              placeholderTextColor="#ffffff"
              keyboardType="email-address"
              autoCapitalize="none"
              onChangeText={email => this.setState({ email })}
            />
            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Password"
              placeholderTextColor="#ffffff"
              secureTextEntry={true}
              autoCapitalize="none"
              onChangeText={password => this.setState({ password })}
            />
            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Confirm Password"
              placeholderTextColor="#ffffff"
              secureTextEntry={true}
              autoCapitalize="none"
              onChangeText={confirmPassword =>
                this.setState({ confirmPassword })
              }
            />
            <TouchableOpacity style={styles.button} onPress={this.register}>
              <Text style={styles.buttonText}>CREATE ACCOUNT</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

export default Signup;

import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1
  },

  backgroundImage: {
    position: "absolute",
    width: "100%",
    height: "100%"
  },

  logocontainer: {
    paddingTop: 110,
    paddingBottom: 20,
    alignItems: "center",
    justifyContent: "center"
  },

  formcontainer: {
    //  padding: 20,
    alignItems: "center",
    justifyContent: "center"
  },

  input: {
    width: 300,
    height: 50,
    backgroundColor: "rgba(255,255,255,0)",
    fontSize: 16,
    color: "#ffffff",
    marginBottom: 20,
    borderColor: "#FFA734",
    borderWidth: 2,
    borderRadius: 10,
    paddingLeft: 15
  },

  button: {
    width: 200,
    height: 45,
    marginBottom: 10,
    backgroundColor: "#FFA734",
    alignItems: "center",
    justifyContent: "center"
  },

  buttonText: {
    color: "#ffffff",
    fontWeight: "bold",
    fontSize: 18
  }
});

export default styles;

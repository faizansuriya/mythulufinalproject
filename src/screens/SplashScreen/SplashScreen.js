import React, {Component} from 'react';
import { View, Image } from 'react-native';
import styles from './splashScreenStyle';

const _backgroundImage = require("../../resources/assets/splash-background.png");
const _logoImage = require("../../resources/assets/logo.png");


class Splash extends Component {
    
    render() {
        return (
            <View style={styles.container}>
            <Image source={_backgroundImage}
              style={styles.backgroundImage}>
            </Image>
              <View style={styles.logoContainer}>
               <Image source={_logoImage}>
              </Image>
             </View>
             </View>
        );
    }
}

export default Splash;



  
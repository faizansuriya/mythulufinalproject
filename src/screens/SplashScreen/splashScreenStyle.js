import {StyleSheet} from 'react-native';

const styles =  StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#000000'
  
    },
  
    backgroundImage: {
      flex: 1,
      position: 'absolute',
      width: '100%',
      height: '100%',
    },
  
    logoContainer:{
  
         flex: 1,
         alignItems: 'center',
         justifyContent: 'center'
       }
});

export default styles;
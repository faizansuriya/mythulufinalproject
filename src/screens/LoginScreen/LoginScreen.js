import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  AsyncStorage,
  StyleSheet
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import styles from "./loginScreenStyle";
import Prompt from "rn-prompt";
//import prompt from 'react-native-prompt-android';
import { emailConstraint, passwordConstraint } from "../../utils/constraints";
import * as generalStyles from "../../styles/generalStyle";
import * as authUtils from "../../utils/auth.util";
import services from "../../services";
import { get } from "lodash";
//import { emailConstraint, passwordConstraint } from "../../validators/validation";
//import Joi from 'joi';
var validate = require("validate.js");

const _backgroundImage = require("../../resources/assets/splash-background.png");
const _logoImage = require("../../resources/assets/logo.png");

import { loginUser } from "../../services/users";
//import { setUserInfo } from '../commons/userAsyncStorage';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      promptVisible: false
    };
  }

  async componentDidMount() {
    const loginStatus = await authUtils.isLoggedIn();
    const token = await authUtils.getToken();
    if (loginStatus) this.props.navigation.navigate("profileScreen");
    AsyncStorage.clear();
  }

  // SignUp Start
  _signIn = async () => {
    var constraints = {
      email: emailConstraint,
      password: passwordConstraint
    };
    var validation = validate(
      { email: this.state.email, password: this.state.password },
      constraints,
      { format: "flat" }
    );
    //   console.log(validation);
    if (validation) {
      alert(validation[0]);
      return;
    }

    // if (!this.state.email || !this.state.password) {
    //     alert("Invalid Email & Password");
    // }
    try {
      const token = await services.auth.loginUser(this.state);
      await authUtils.setToken(token);
      if (token.data.status) {
        this.props.navigation.navigate("profileScreen");
      } else {
        alert(get(token, "data.error.message", "Invalid Credentials"));
      }
    } catch (err) {
      console.log(err);
    }
    // if (!this.state.email || !this.state.password) {
    //     alert("Invalid Email & Password");
    // }
    // else {
    //     loginUser(this.state).then((res) => {
    //         if (res.data.status) {
    //             const val = authUtils.setToken(res);
    //             console.log("The Value of Val:", val);
    //             this.props.navigation.navigate("profileScreen");
    //         }

    //         else {
    //             alert(get(res, 'data.error.message', 'Invalid Credentials'));
    //         }

    //     }).catch((err) => {
    //         console.log(err);
    //     });

    // }
  };

  _createAccount = () => {
    this.props.navigation.navigate("signupScreen");
  };

  _forgetPassword = async email => {
    await this.setState({ promptVisible: false });

    try {
      const response = await services.auth.forgetPassword(email);
      await authUtils.setAsyncStorage("reset", response);
      console.log(">>>", response);
      alert(response.data.response.message);
      this.props.navigation.navigate("resetPasswordScreen", { email: email });
    } catch (error) {
      console.log(error.response);
      alert(error.response.data.error.message);
    }

    // if (response.response.status === 401) {
    //   alert(response.error.message);
    // } else {
    //   alert(response.data.message);
    //   this.props.navigation.navigate("resetPasswordScreen");
    // }
    // fetch("http://159.89.41.180/mythulu/api/v1/forgot_password", {
    //   method: "POST",
    //   headers: {
    //     Accept: "application/json",
    //     "Content-Type": "application/json"
    //   },

    //   body: JSON.stringify({
    //     email
    //   })
    // })
    //   .then(response => response.json())
    //   .then(res => {
    //     if (res.status) {
    //       alert(res.response.message);
    //       this.props.navigation.navigate("resetPasswordScreen");
    //     } else {
    //       //alert(JSON.stringify(res));
    //       console.log(res);
    //       alert(res.error.message);
    //     }
    //   })
    //   .catch(err => {
    //     console.log(err);
    //     // alert(err)
    //   });
  };

  // SignUp End

  //     _loadInitialState = async () => {
  //      try{
  //         let value  =  await AsyncStorage.getItem('user');
  //         if(value != null){
  //            // const newvalue = JSON.parse(value)
  //            const details = JSON.parse(value);
  //            console.log(details.name);
  //            this.props.navigation.navigate("profileScreen")
  //         }
  //     }
  //     catch(error){
  //         alert(error);
  //     }

  // }

  //     componentDidMount(){
  //         this._loadInitialState();
  //     }

  // signIn = () => {

  //     // alert(this.state.email);

  //     fetch ('http://159.89.41.180/mythulu/api/v1/login' , {
  //         method : 'POST',
  //         headers: {
  //             'Accept' : 'application/json',
  //             'Content-Type' : 'application/json',
  //         },

  //         body: JSON.stringify ({
  //             type: 'normal',
  //             email: this.state.email,
  //             password: this.state.password,
  //         })
  //     })

  //     .then((response) => response.json())
  //     .then ((res) => {

  //         if(res.status === true) {
  //              AsyncStorage.setItem('user', res);
  //              this.props.navigation.navigate('profileScreen');
  //         }

  //         else{
  //             //alert(JSON.stringify(res));
  //             alert("status False")
  //         }
  //     })
  //     .catch((err) => {
  //         console.log(err)
  //         // alert(err)
  //     });
  // }

  render() {
    return (
      <View style={styles.container}>
        <Image source={_backgroundImage} style={styles.backgroundImage} />
        <KeyboardAwareScrollView>
          <View style={styles.logocontainer}>
            <Image source={_logoImage} />
          </View>
          <View style={styles.formcontainer}>
            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Email"
              placeholderTextColor="#ffffff"
              keyboardType="email-address"
              autoCapitalize="none"
              onChangeText={email => this.setState({ email })}
            />

            <TextInput
              style={styles.input}
              underlineColorAndroid="transparent"
              placeholder="Password"
              placeholderTextColor="#ffffff"
              secureTextEntry={true}
              autoCapitalize="none"
              onChangeText={password => {
                console.log(password);
                this.setState({ password });
              }}
            />

            <TouchableOpacity style={styles.button} onPress={this._signIn}>
              <Text style={styles.buttonText}>LOG IN</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.button}
              onPress={this._createAccount}
            >
              <Text style={styles.buttonText}>CREATE ACCOUNT</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.passwordLink}
              onPress={() => this.setState({ promptVisible: true })}
            >
              <Text style={styles.passwordLinkText}>FORGET PASSWORD</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.otherlogincontainer}>
            <Text style={styles.logintext}> Log in with </Text>

            <TouchableOpacity style={styles.link}>
              <Text
                style={styles.linktext}
                onPress={() => Linking.openURL("https://google.com")}
              >
                Facebook
              </Text>
            </TouchableOpacity>

            <Text style={styles.logintext}> or </Text>

            <TouchableOpacity style={styles.link}>
              <Text
                style={styles.linktext}
                onPress={() => Linking.openURL("https://google.com")}
              >
                Google
              </Text>
            </TouchableOpacity>
          </View>
          <Prompt
            title="Forget Password?"
            placeholder="Please enter your email"
            defaultValue=""
            visible={this.state.promptVisible}
            onCancel={() => this.setState({ promptVisible: false })}
            onSubmit={input => this._forgetPassword(input)}
          />
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

export default Login;

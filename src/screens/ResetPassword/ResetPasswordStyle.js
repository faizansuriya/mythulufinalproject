import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 54,
    backgroundColor: "red"
  },
  formContainer: {
    flex: 1,
    backgroundColor: "white"
  },
  headingText: {
    paddingTop: 20,
    paddingLeft: 20,
    fontSize: 20,
    fontWeight: "bold",
    color: "black"
  },
  saveButton: {
    width: 200,
    height: 45,
    marginBottom: 10,
    backgroundColor: "#FFA734",
    alignItems: "center",
    justifyContent: "center"
  },

  saveButtonText: {
    color: "#ffffff",
    fontWeight: "bold",
    fontSize: 18
  },
  textinput: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10
  }
});

export default styles;

import React, { Component } from "react";
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  AsyncStorage
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import styles from "./ResetPasswordStyle";
import services from "../../services";
import * as authUtils from "../../utils/auth.util";
//import { View, Text, TextInput, Button, NavigationBar } from "@shoutem/ui";

class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: "",
      password: "",
      email: ""
    };
  }

  // async componentDidMount() {
  //   const response = await authUtils.getAsyncStorage("reset");
  //   const resetEmail = response.config.data;
  //   console.log({ resetEmail });
  //   this.setState.email = { resetEmail };
  // }

  _resetPassword = async () => {
    const params = {
      code: this.state.code,
      password: this.state.password,
      email: this.props.navigation.state.params.email
    };
    console.log("params", params);
    try {
      const response = await services.auth.resetPassword(params);
      console.log(response);
    } catch (error) {
      console.log(">>>", error.response);
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.formContainer}>
          <Text style={styles.headingText}>VERIFICATION CODE</Text>
          <TextInput
            style={styles.textinput}
            placeholder="Enter code"
            autoCapitalize="none"
            onChangeText={code => {
              console.log(code);
              this.setState({ code });
            }}
          />
          <Text style={styles.headingText}>NEW PASSWORD</Text>
          <TextInput
            style={styles.textinput}
            secureTextEntry={true}
            placeholder="Enter Your New Password"
            autoCapitalize="none"
            onChangeText={password => {
              console.log(password);
              this.setState({ password });
            }}
          />
          <TextInput
            style={styles.textinput}
            secureTextEntry={true}
            placeholder="Retype Your New Password"
            autoCapitalize="none"
            onChangeText={confirmPassword => {
              console.log(confirmPassword);
              this.setState({ confirmPassword });
            }}
          />
          <TouchableOpacity
            style={styles.saveButton}
            onPress={this._resetPassword}
          >
            <Text style={styles.saveButtonText}>SAVE</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default ResetPassword;

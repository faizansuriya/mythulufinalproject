import React, {Component} from 'react';
import { View, Text, AsyncStorage } from 'react-native';
import styles from './ProfileScreenStyle';



class Profile extends Component {

    state = {
        user : {}
    }

    _loadInitialState = async () => {
        try {
           const res = await AsyncStorage.getItem('user');
           const user = JSON.parse(res);
           return user;
       }
       catch(error){
           alert(error);
       }
    
    }
    
       async componentDidMount(){
           const user = await this._loadInitialState();
           this.setState( {user} );
       }

    render() {
    return (
            <View style={styles.container}>
                <Text>this is profile screen</Text>
            </View>
        );
    }
}

export default Profile;
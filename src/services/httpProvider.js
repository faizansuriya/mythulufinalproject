import axios from "axios";

const API_VER = "v1";
const BASE_URL = `http://159.89.41.180/mythulu/api/${API_VER}`;

// axios.interceptors.response.use(
//   function(response) {
//     return response;
//   },
//   function(error) {
//     console.log("error 401", error);
//     if (401 == error.response.status) {
//       return {
//         status: false,
//         response: {
//           message: "Error Occured"
//         }
//       };
//     } else {
//       console.log("else");
//       return Promise.reject(error);
//     }
//   }
// );

// const instance = axios.create({
//   baseURL: BASE_URL,
//   timeout: 5000,
//   headers: { "Content-Type": "application/json" }
// });

export function get({ url }) {
  return axios.get(`${BASE_URL}/${url}`);
}

export function post({ url, body }) {
  return axios.post(`${BASE_URL}/${url}`, body);
  // try {
  //   return instance.post(`/${url}`, body);
  // } catch (error) {
  //   console.log(error);
  // }
}

export function put({ url, body }) {
  return axios.put(`${BASE_URL}/${url}`, body);
}

export function remove({ url }) {
  return axios.remove(`${BASE_URL}/${url}`);
}

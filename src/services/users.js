import { post, get, put, remove } from "./httpProvider";

const API_VER = "v1";
const BASE_URL = `http://159.89.41.180/mythulu/api/${API_VER}`;

const loginUser = ({ email, password }) => {
  return post({
    url: "login",
    body: {
      type: "normal",
      email: email,
      password: password
    }
  });
};

const registerUser = ({ name, email, password }) => {
  return post({
    url: "register",
    body: {
      name: name,
      email: email,
      password: password
    }
  });
};

const forgetPassword = email => {
  return post({
    url: "forgot_password",
    body: {
      email: email
    }
  });
  //   return response;
  // } catch (e) {
  //   console.log("error", e);
  //   return {
  //     data: {
  //       response: {
  //         message: "Incorrect Email"
  //       },
  //       status: false
  //     }
  //   };
  // }
};

const resetPassword = ({ code, email, password }) => {
  return post({
    url: "verify_code",
    body: {
      code: code,
      email: email,
      password: password
    }
  });
};

// export function getUserData() {
//     return get({
//         url: 'login'
//     })
// }

const authServices = {
  loginUser,
  registerUser,
  forgetPassword,
  resetPassword
  // fetchForgetPassword
};

export default authServices;

// // axios
// function httpGet({ url, options, qs = ''}) {
//     return axios.get(`${url}?${qs`});
// }

// request
// function httpGet({ url, options, qs = ''}) {
//     return request(url', function (error, response, body) {
//         if (!error) {
//             return response;
//         }
//         console.log('error:', error); // Print the error if one occurred
//         console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
//         console.log('body:', body); // Print the HTML for the Google homepage.
//     });
// }

// function httpGet(params) {
//     const { url, options, qs = ''} = params;
//     const headerOpts = {
//         'Accept' : 'application/json',
//         'Content-Type' : 'application/json',
//         ...options
//     }

//     return fetch (`${url}?${qs}` , {
//             method : 'GET',
//             headers: headerOpts
//         })

//         .then((response) => response.json())
//         .then ((res) => res)
//         .catch((err) => {
//             console.log(err)
//             // alert(err)
//         });
// }

// function httpPost(params) {
//   const { url, body, options } = params;
//   const headerOpts = {
//     Accept: "application/json",
//     "Content-Type": "application/json",
//     ...options
//   };

//   return fetch(url, {
//     method: "POST",
//     headers: headerOpts,
//     body: body
//   })
//     .then(response => response.json())
//     .then(res => res)
//     .catch(err => {
//       console.log(err);
//       // alert(err)
//     });
// }

// function getUsersList({ qs }) {
//     return httpGet({
//         url: `${baseUrl}/users`,
//     })
// }

// function fetchForgetPassword({ email }) {
//   return httpPost({
//     url: `${BASE_URL}/forgot_password`,
//     body: JSON.stringify({
//       //type: 'normal',
//       email: email
//       //password: this.state.password,
//     })
//   });
// }

import React, { Component } from "react";
import { createStackNavigator } from "react-navigation";
import Login from "../screens/LoginScreen";
import Profile from "../screens/ProfileScreen";
import Signup from "../screens/SignUpScreen";
import ResetPassword from "../screens/ResetPassword";

export default createStackNavigator({
  loginScreen: {
    screen: Login,
    navigationOptions: { header: null }
  },
  profileScreen: {
    screen: Profile,
    navigationOptions: { header: null }
  },
  signupScreen: {
    screen: Signup,
    navigationOptions: { header: null }
  },
  resetPasswordScreen: {
    screen: ResetPassword,
    navigationOptions: {
      title: "Reset Password"
    }
  }
});

import localStoreUtil from "../utils/localstore.util";

export const getToken = () => localStoreUtil.get_Data("token");
export const setToken = token => localStoreUtil.store_Data("token", token);

export const getAsyncStorage = key => localStoreUtil.get_Data(key);
export const setAsyncStorage = (key, val) =>
  localStoreUtil.store_Data(key, val);

export const isLoggedIn = async () => {
  const token = await getToken();
  const response = token == null ? false : true;
  return Promise.resolve(response);
};

import { AsyncStorage } from 'react-native';

const localStoreUtil = {
    store_Data: async (key, data) => {
        await AsyncStorage.setItem(key, JSON.stringify(data));
        return true;
    },

    get_Data: async key => {
        const item = await AsyncStorage.getItem(key);
        if (!item) return;

        return JSON.parse(item);

    },

    remove_Data: async (key) => {

    },

    remove_All: async () => {

    }
}


export default localStoreUtil;
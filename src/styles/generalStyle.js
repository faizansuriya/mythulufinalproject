import { StyleSheet } from "react-native";


export const container = StyleSheet.create({
    style: {
        display: "flex",
        flexDirection: "row",
        flex: 1,
        padding: 5,
        backgroundColor: "white"
    }

});

export const content = StyleSheet.create({
    style: {
        display: "flex",
        flex: 1,
        flexDirection: "column"

    }
});
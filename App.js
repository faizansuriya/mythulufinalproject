import React from "react";
import Splash from "./src/screens/SplashScreen";
import RootStack from "./src/routes";

class App extends React.Component {
  state = {
    showSplash: true
  };

  _hideSplashScreen = () => {
    this.setState(() => ({ showSplash: false }));
  };

  componentDidMount() {
    setTimeout(this._hideSplashScreen, 1000);
  }

  render() {
    const { showSplash } = this.state;
    return showSplash ? <Splash /> : <RootStack />;
  }
}

export default App;
